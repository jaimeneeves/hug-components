# Documentação

Ferramenta para criar/renderizar components a partir de códigos extraídos do Figma.

## Fluxo Principal

| Ordem        | Descrição      |
| ------------ |:-------------|
| 1      | Desenhar no Figma |
| 2      | Extrair os tokens (cores, fontes, espaços, etc) do arquivo Figma (via Theo, por exemplo)      |
| 3 | Usar esse JSON para alimentar uma galeria de componentes agnóstica |
| 4 | Pegar componentes agnósticos e transformar para a linguagem que for necessária (Android, iOS, Vue, React...)|

## Referências

[Style-dictionary](https://github.com/amzn/style-dictionary)

[React-styleguidist](https://github.com/styleguidist/react-styleguidist)

[Themer](https://www.figma.com/community/plugin/731176732337510831/Themer)

[Interplayapp](https://interplayapp.com/)

### Vídeos

[Thomas Lowry](https://www.youtube.com/watch?v=aM8SkksBy3Y)

## Technologies

* Webpack
* Babel

```
├── src/
│   ├── components/
│   │  ├── nav/
│   │  │  ├── nav.js
│   │  │  ├── nav.html
│   │  │  └── directory/
│   │  │     ├── file1.js
│   │  │     └── file2.scss
│   │  ├── events/
│   │  │  ├── events.module.js
│   │  │  └── events-signup/
│   │  │     ├── events-signup.module.js
│   │  │     └── events-signup.scss
│   │  └── components.module.js
│   └── app.scss
└── index.html
```