import { Message } from './utils';

/**
 * Class to set up Nav
 */
class HugNav {

  /**
   * @param {Object} payload
   * @param {Object} options
   */
  constructor(payload) {
    // For rendering default
    this._el = null;

    // Get template for rendering
    this.template = payload.template ? payload.template : null;

    if(!payload.data) throw new Error(Message.NOPAYLOAD);

    this.prepare(payload.data);
  }

  /**
   * Prepare HugNav
   * @param {Object} data
   */
  prepare(data) {
    let _container = null;

    if( typeof this.template === 'string') {

      _container = document.createElement('div');

      let str = '';
      let contentChildren = '';
      data.forEach(item => {
        // Get keys from item
        let dataKeys = Object.keys(item);

        // Get template
        let HtmlItem = this.template;

        dataKeys.forEach(key => {
          contentChildren = HtmlItem.replace(`@${key}`, item[key]);
          HtmlItem = contentChildren;
        });
        str += contentChildren;
      });
      _container.innerHTML = str;
      this._el = _container;
    } else {

      if(typeof this.template.container === 'object') {

        if(!this.template.container.el) throw new Error(Message.NO_ELEMENT);

        _container = document.createElement(this.template.container.el);

        /**
         * [Attributes] on Container HTMLElement
         */
        if(typeof this.template.container.attrs === 'object') {
          const attrs = this.template.container.attrs;
          const keysAttrs = Object.keys(attrs);

          keysAttrs.forEach(attrKey => {
            _container.setAttribute(attrKey, attrs[attrKey]);
          });
        }
        // <!-- End [Attributes] -->

        // Children elements
        if(typeof this.template.children === 'object') {
          _container = this.chidElObject(data, _container);
        }

        this._el = _container;
      }
    }
  }

  /**
   * For object type child element
   * @param {Object} data
   * @param {HTMLElement} _container
   */
  chidElObject(data, _container) {

    // Element children
    const _elChildren = document.createElement(this.template.children.el);

    // Content children
    const content = this.template.children.content;

    // [Attributes] on children element
    let attrsKeys = [];
    let attrs = {};

    if(typeof this.template.children.attrs === 'object') {
      attrs = this.template.children.attrs;
      attrsKeys = Object.keys(attrs);
    }

    _elChildren.innerHTML = `${content}`;

    data.forEach(item => {
      // Clone the children element
      let _elChildrenClone = _elChildren.cloneNode(true);

      // Get keys from item
      let dataKeys = Object.keys(item);

      let str = _elChildren.innerHTML;

      let contentChildren = '';
      dataKeys.forEach(key => {
        contentChildren = str.replace(`@${key}`, item[key]);
        str = contentChildren;

        // Attrs
        attrsKeys.forEach(attrKey => {
          let content = attrs[attrKey];

          if(content.indexOf(key) !== -1) {
            let strKey = content.replace(`@${key}`, item[key]);
            _elChildrenClone.setAttribute(attrKey, strKey);
          }
        });
        // End Attrs
      });

      _elChildrenClone.innerHTML = contentChildren;
      _container.append(_elChildrenClone);
    });

    return _container;
  }

  render() {
    return this._el;
  }
}

export default HugNav;
