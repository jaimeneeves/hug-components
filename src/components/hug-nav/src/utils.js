export const Message = {
  NOPAYLOAD: 'No payload to set up menu!',
  CHILDREN_NOT_COMPATIBLE: 'Children not compatible',
  NO_ELEMENT: 'Without HTML Element'
};
