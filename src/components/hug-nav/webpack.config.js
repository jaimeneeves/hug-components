const path = require('path');

const DIST = 'dist';

module.exports = {
  mode: 'development',
  entry: './src/hug-nav.js',
  output: {
    library: 'HugNav',
    libraryTarget: 'umd',
    libraryExport: 'default',
    filename: 'hug-nav.js',
    path: path.resolve(__dirname, DIST),
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};
