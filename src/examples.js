import HugNav from './components/hug-nav/src/hug-nav';

const menu = {
  data: [
    {
      title: 'Monitor',
      url: '#/monitor',
      icon: 'icon-camera3',
      classI: 'monitor',
      dataTitle: 'Monitor',
      items: []
    },
    {
      title: 'Tickets',
      url: '#/tickets',
      icon: 'icon-ticket',
      classI: 'tickets',
      dataTitle: 'Tickets',
      items: []
    },
    {
      title: 'Social-CRM',
      url: '#/socialcrm',
      icon: 'icon-users',
      classI: 'socialcrm',
      dataTitle: 'social CRM',
      items: []
    },
    {
      title: 'Mensagens',
      url: '#/mensagem/',
      icon: 'icon-bubble',
      classI: 'alerta',
      dataTitle: 'mensagens',
      items: []
    }
  ],

  // Templates

  // template: `
  // <a ng-href="@url" class="hint--top" data-title="@dataTitle">
  //   <i class="@icon"></i>
  //   <span>@title</span>
  // </a>`

  template: {

    container: {
      el: 'ul',
      
      // attrs: {
      //   class: 'item-main'
      // }
    },

    children: {
      // Children element
      el: 'li',

      attrs: {
        class: 'm-@classI',
        'ng-class': `{active: currentMenuActive == '@classI'}`
      },

      // Key from [data]
      content: `
      <a ng-href="@url" class="hint--top" data-title="@dataTitle">
        <i class="@icon"></i>
        <span>@title</span>
      </a>`
    }
  }
};

/**
 * HugNav
 */
const hugNav = new HugNav(menu);
const elNav = hugNav.render();

// console.log(elNav.outerHTML);
console.log(elNav);
