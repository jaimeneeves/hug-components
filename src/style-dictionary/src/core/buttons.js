const colors = require('./colors');

module.exports = {
  /**
   * Get buttons styles
   */
  getButtonStyles: (styles) => {
    // empty "spacers obj" wheree we will store all colors
    const resultStyles = {};

    styles.map((item, i) => {
      if (item.children) {
        let subItems = {};

        if(item.name === "btn") {
          item.children.map(subItem => {
            var borderType = "";
            var margin = {
              x: { "value": 0 },
              y: { "value": 0 }
            };

            if(subItem.strokes.length > 0 ) {
              borderType = `${subItem.strokes[0].type.toLowerCase()}`;
            }

            if(item.verticalPadding) {
              margin.x.value = `${item.verticalPadding}px`
            }

            if(item.horizontalPadding) {
              margin.y.value = `${item.horizontalPadding}px`
            }

            let subFontObj = {
              'font-family': {
                value: `${subItem.children[0].style.fontFamily}`,
                type: "button-default"
              },
              'font-weight': {
                value: `${subItem.children[0].style.fontWeight}`,
                type: "button-default"
              },
              'font-size': {
                value: `${subItem.children[0].style.fontSize}px`,
                type: "button-default"
              },
              'line-height': {
                value: `${subItem.children[0].style.lineHeightPx}px`,
                type: "button-default"
              },
              'border-radius':{
                value:`${subItem.cornerRadius}px`,
                type: "button-default"
              },
              'border':{
                value:`${subItem.strokeWeight}px ${borderType}`,
                type: "button-default"
              },
              'padding':{
                'x': {
                  "value": `${subItem.horizontalPadding}px` 
                },
                'y': { 
                  "value":`${subItem.verticalPadding}px`
                }
              },
              'margin': margin
            };
            // merge multiple subfonts objects into one
            Object.assign(subItems, subFontObj);
          });
        }

        // if(item.name === "primary") {
        //   item.children.map(itemPrimary => {
        //     var borderType = "";
        //     if(itemPrimary.strokes.length > 0 ) {
        //       borderType = `${itemPrimary.strokes[0].type.toLowerCase()}`;
        //     }

        //     let subFontObj = {
        //       'border':{
        //         value:`${itemPrimary.strokeWeight}px ${borderType}`,
        //         type: "button-primary"
        //       }
        //     };
        //     // merge multiple subfonts objects into one
        //     Object.assign(subItems, subFontObj);
        //   });
        // }

        // if(item.name === "secondary") {
        //   item.children.map(items => {
        //     let subFontObj = {};
        //     // merge multiple subfonts objects into one
        //     // Object.assign(subItems, subFontObj);
        //     Object.assign(subItems, colors.extractColor(items));
        //   });
        // }

        // if(item.name === "success") {
        //   item.children.map(items => {
        //     let subFontObj = {};
        //     // merge multiple subfonts objects into one
        //     // Object.assign(subItems, subFontObj);
        //     Object.assign(subItems, colors.extractColor(items));
        //   });
        // }

        let fontObj = {
          [item.name]: subItems
        };

        Object.assign(resultStyles, fontObj);
      }
    });

    return resultStyles;
  }
};
