
const prepareColor = (itemColor) => {
  
  function rbaObj(obj) {
    return itemColor.fills[0].color[obj] * 255;
  }

  const colorObj = {
    [itemColor.name.toLowerCase()]: {
      value: `rgba(${rbaObj("r")}, ${rbaObj("g")}, ${rbaObj("b")},${itemColor.fills[0].color.a})`,
      type: "color"
    }
  };

  return colorObj;
};

module.exports = {

  extractColor: prepareColor,

  /**
   * Get colors
   */
  getPaletteRaColors: (styles) => {
    // empty "palette obj" wheree we will store all colors
    const palette = {};
  
    // get colors from each children
    styles.map(item => {

      if(item.hasOwnProperty('children')) {
        item.children.map(subItem => {
          Object.assign(palette, prepareColor(subItem));
        });
      } else {
        Object.assign(palette, prepareColor(item));
      }
    });

    // let colorMain = {
    //   primary: {
    //     value: "{color.bowl.value}",
    //     type: "color"
    //   },
    //   secondary: {
    //     value: "{color.ranking-litchi.value}",
    //     type: "color"
    //   },
    //   success: {
    //     value: "{color.ranking-avocado.value}",
    //     type: "color"
    //   },
    //   info: {
    //     value: "{color.ranking-grapes.value}",
    //     type: "color"
    //   },
    //   warning: {
    //     value: "{color.ranking-pineapple.value}",
    //     type: "color"
    //   },
    //   danger: {
    //     value: "{color.ranking-apple.value}",
    //     type: "color"
    //   },
    //   white: {
    //     value: "{color.ice.value}",
    //     type: "color"
    //   }
    // };

    // Object.assign(palette, colorMain);
    return palette;
  }
};
