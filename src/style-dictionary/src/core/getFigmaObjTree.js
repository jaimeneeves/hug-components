const axios = require('axios');
const jsonfile = require('jsonfile');
const buttons = require('./buttons');
const colors = require('./colors');

const figmaApiKey = '52216-2bc71a90-a26c-4765-84a3-814447560d07';
const figmaFileId = 'JYEgReQEh6QUvtVVreWjr9';
const filePathJSON = './src/base.json';

function getGrids(stylesArtboard) {
  // empty "grids obj" wheree we will store all colors
  const grids = {};
  // get "grids" artboard
  const gridsAtrboard = stylesArtboard.filter(item => {
    return item.name === "grids";
  })[0].children;

  gridsAtrboard.map(item => {
    gridObj = {
      [item.name]: {
      count: {
        value: item.layoutGrids[0].count,
        type: "grids"
      },
      gutter: {
        value: `${item.layoutGrids[0].gutterSize}px`,
        type: "grids"
      },
      offset: {
        value: `${item.layoutGrids[0].offset}px`,
        type: "grids"
      }
      }
    };

    Object.assign(grids, gridObj);
  });

  return grids;
}

function getPalette(stylesArtboard) {
  // empty "palette obj" wheree we will store all colors
  const palette = {};
  // get "palette" artboard
  const paletteAtrboard = stylesArtboard.filter(item => {
    return item.name === "palette";
  })[0].children;

  // get colors from each children
  paletteAtrboard.map(item => {
    function rbaObj(obj) {
      return item.fills[0].color[obj] * 255;
    }

    colorObj = {
      [item.name]: {
        value: `rgba(${rbaObj("r")}, ${rbaObj("g")}, ${rbaObj("b")},${item.fills[0].color.a})`,
        type: "color"
      }
    };

    Object.assign(palette, colorObj);
  });

  return palette;
}

function getSpacers(stylesArtboard) {
  // empty "spacers obj" wheree we will store all colors
  const spacers = {};
  // get "spacers" artboard
  const spacersAtrboard = stylesArtboard.filter(item => {
    return item.name === "spacers";
  })[0].children;

  spacersAtrboard.map(item => {
  const spacerObj = {
    [item.name]: {
      value: `${item.absoluteBoundingBox.height}px`,
      type: "spacers"
    }
  };

  Object.assign(spacers, spacerObj);
  });

  return spacers;
}

function getFontStyles(stylesArtboard) {
  // empty "spacers obj" wheree we will store all colors
  const fontStyles = {};
  // get "spacers" artboard
  const fontStylesAtrboard = stylesArtboard.filter(item => {
    return item.name === "typography";
  })[0].children;

  fontStylesAtrboard.map((fontItem, i) => {
    if (fontItem.children) {
      let subFonts = {};

      // get all sub fonts
      fontItem.children.map(subFontItem => {
        let subFontObj = {
          [subFontItem.name]: {
            family: {
              value: `${subFontItem.style.fontFamily}`,
              type: "typography"
            },
            size: {
              value: `${subFontItem.style.fontSize}px`,
              type: "typography"
            },
            weight: {
              value: subFontItem.style.fontWeight,
              type: "typography"
            },
            lineheight: {
              value: `${subFontItem.style.lineHeightPercent}%`,
              type: "typography"
            },
            spacing: {
              value:
                subFontItem.style.letterSpacing !== 0
                  ? `${subFontItem.style.letterSpacing}px`
                  : "normal",
              type: "typography"
            }
          }
        };
        // merge multiple subfonts objects into one
        Object.assign(subFonts, subFontObj);
      });

      //
      let fontObj = {
        [fontItem.name]: subFonts
      };

      Object.assign(fontStyles, fontObj);
    } else {
      let fontObj = {
        [fontItem.name]: {
          family: {
            value: `${fontItem.style.fontFamily}, ${
              fontItem.style.fontPostScriptName
            }`,
            type: "typography"
          },
          size: {
            value: fontItem.style.fontSize,
            type: "typography"
          },
          weight: {
            value: fontItem.style.fontWeight,
            type: "typography"
          },
          lineheight: {
            value: `${fontItem.style.lineHeightPercent}%`,
            type: "typography"
          },
          spacing: {
            value: fontItem.style.letterSpacing !== 0 ? `${fontItem.style.letterSpacing}px` : "normal",
            type: "typography"
          }
        }
      };

      Object.assign(fontStyles, fontObj);
    }
  });

  return fontStyles;
}

// Main

async function getFigmaObjTree(figmaApiKey, figmaId) {
  let result = await axios.get("https://api.figma.com/v1/files/" + figmaId, {
    method: "GET",
    headers: {
      "X-Figma-Token": figmaApiKey
    }
  });
  let figmaTreeStructure = result.data;

  /**
   * Get Pages From Figma
   */

  // const stylesArtboard = figmaTreeStructure.document.children.filter(item => {
  //   return item.name === "styles";
  // })[0].children;

  const pageColors = figmaTreeStructure.document.children.filter(item => {
    return item.name === "Colors";
  })[0].children;

  const pageButtons = figmaTreeStructure.document.children.filter(item => {
    return item.name === "Buttons";
  })[0].children;

  baseTokeensJSON = {
    grids: {},
    spacers: {},
    fonts: {},
    color: {},
    button:{}
  };

  // Object.assign(baseTokeensJSON.token.grids, getGrids(stylesArtboard));
  // Object.assign(baseTokeensJSON.token.spacers, getSpacers(stylesArtboard));
  // Object.assign(baseTokeensJSON.token.fonts, getFontStyles(stylesArtboard));
  // Object.assign(baseTokeensJSON.colors, getPalette(pageColors));
  Object.assign(baseTokeensJSON.color, colors.getPaletteRaColors(pageColors));
  Object.assign(baseTokeensJSON.button, buttons.getButtonStyles(pageButtons));
  return baseTokeensJSON;
}

async function generateJSON() {
  const obj = await getFigmaObjTree(figmaApiKey,figmaFileId);
  const file = filePathJSON;

  jsonfile.writeFile(file, obj, { spaces: 2 })
    .then(res => {
      console.log('-----------------');
      console.log('Write complete');
    })
    .catch(error => console.error(error))

}

generateJSON();