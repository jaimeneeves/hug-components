const StyleDictionary = require("style-dictionary").extend({
  source: ["src/**/*.json"],
  platforms: {
    scss: {
      transformGroup: "scss",
      buildPath: "src/scss/",
      files: [
        {
          destination: "themes/_color.scss",
          format: "scss/variables",
          filter: {
            type: "color"
          }
        },
        {
          destination: "_variables.scss",
          format: "scss/variables",
          filter: function(prop) {
            return prop.attributes.category !== "color";
          }
        }
        // {
        //   destination: "colors/_palette.scss",
        //   format: "scss/variables",
        //   filter: {
        //     type: "color"
        //   }
        // },
        // {
        //   destination: "_typography.scss",
        //   format: "scss/variables",
        //   filter: {
        //     type: "typography"
        //   }
        // },
        // {
        //   destination: "_grids.scss",
        //   format: "scss/variables",
        //   filter: {
        //     type: "grids"
        //   }
        // },
        // {
        //   destination: "_spacers.scss",
        //   format: "scss/variables",
        //   filter: {
        //     type: "spacers"
        //   }
        // },
      ]
    }
  }
});

StyleDictionary.buildAllPlatforms();

console.log("done!");