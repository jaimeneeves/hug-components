import React from 'react';
import "../node_modules/bootstrap/scss/bootstrap.scss";
import './App.css';
import './scss/main.scss';
import Button from './components/Button/Button.js'

function App() {
  var divStyle = {
    marginTop: '70px'
  };

  return (
    <div className="App">
      <h3 className="text-center">ReactJS</h3>

      <div className="container">
        <div className="row">
          <div className="col-sm">
            <div className="card">
              <div className="card-body">    
                <form>
                  <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                  </div>
                  <div className="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword1"/>
                  </div>
                  <div className="form-group form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                    <label className="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                  <Button type="primary">Submit</Button>
                </form>
              </div>
            </div>
          </div>
          <div className="col-sm">
            <div className="card">
              <div className="card-body">
                <form>
                  <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                  </div>
                  <div className="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword1"/>
                  </div>
                  <Button type="secondary">Submit</Button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div className="row" style={divStyle}>
          <div className="col-sm">
            <div className="card">
              <div className="card-body">
                <form>
                  <div className="form-row align-items-center">
                    <div className="col-auto">
                      <label className="sr-only" for="inlineFormInput">Name</label>
                      <input type="text" className="form-control mb-2" id="inlineFormInput" placeholder="Jane Doe"/>
                    </div>
                    <div className="col-auto">
                      <label className="sr-only" for="inlineFormInputGroup">Username</label>
                      <div className="input-group mb-2">
                        <div className="input-group-prepend">
                          <div className="input-group-text">@</div>
                        </div>
                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Username"/>
                      </div>
                    </div>
                    <div className="col-auto">
                      <div className="form-check mb-2">
                        <input className="form-check-input" type="checkbox" id="autoSizingCheck"/>
                        <label className="form-check-label" for="autoSizingCheck">
                          Remember me
                        </label>
                      </div>
                    </div>
                    <div className="col-auto">
                      <Button type="success">Submit</Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
