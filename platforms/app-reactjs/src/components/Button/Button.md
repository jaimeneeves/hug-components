Primary button:

```jsx
<Button type="primary">Default</Button>
```

Secondary button:

```jsx
<Button type="secondary">
  Secondary
</Button>
```

Success button:

```jsx
<Button type="success">
  Success
</Button>
```

Danger button:

```jsx
<Button type="danger">
  Danger
</Button>
```

Warning button:

```jsx
<Button type="warning">
  Warning
</Button>
```

Info button:

```jsx
<Button type="info">
  Info
</Button>
```
