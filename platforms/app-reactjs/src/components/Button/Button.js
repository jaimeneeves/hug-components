import React from 'react';
import PropTypes from 'prop-types';
import '../../scss/_button.scss';

/**
 * The only true button.
 */
export default function Button({ color, onClick, disabled, children, type }) {
	const styles = { color, };
  const className = `btn btn-${type}`;

	return (
		<button
			className={className}
			style={styles}
			onClick={onClick}
			disabled={disabled}>
			{children}
		</button>
	);
}
Button.propTypes = {
	/** Button label */
	children: PropTypes.node.isRequired,
	/** The color for the button */
	color: PropTypes.string,
	/** Disable button */
	disabled: PropTypes.bool,
	/** Gets called when the user clicks on the button */
	onClick: PropTypes.func,
	/** The type of the button */
	type: PropTypes.oneOf(['primary', 'secondary', 'success', 'danger', 'warning', 'info'])
};
Button.defaultProps = {
	type: 'default',
	onClick: event => {
		// eslint-disable-next-line no-console
		console.log('You have clicked me!', event.target);
	},
};
